declare module 'virtual:icons/*' {
	import type { SvelteComponent } from 'svelte';
	const component: SvelteComponent;
	export default component;
}
