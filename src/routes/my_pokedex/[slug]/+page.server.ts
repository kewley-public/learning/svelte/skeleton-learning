import { createCaller } from '$lib/trpc/router';
import { createContext } from '$lib/trpc/context';

export async function load(event) {
	return {
		pokemon: createCaller(await createContext(event)).pokemon.load(event.params.slug)
	};
}
