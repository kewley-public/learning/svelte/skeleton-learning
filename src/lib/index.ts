export interface PokemonPaginatedResponse {
	next?: string; // https://pokeapi.co/api/v2/pokemon?offset=13&limit=12
	previous?: string; // https://pokeapi.co/api/v2/pokemon?offset=0&limit=1
	count: number;
	results: PokemonItem[];
}

export interface PokemonItem {
	name: string;
	url: string;
}

export interface PokemonApiResponse {
	name: string;
	id: number;
	cries: { latest: string; legacy: string };
	sprites: { front_default: string; front_shiny: string };
	types: { slot: number; type: { name: string; url: string } }[];
}

export interface PokemonData {
	name: string;
	id: number;
	cry: string;
	sprite: string;
	types: string[];
	sprite_shiny: string;
}

export const EMPTY_POKEMON: PokemonData = {
	name: 'Missing No.',
	id: -1,
	cry: '',
	sprite: '',
	types: [],
	sprite_shiny: ''
};
