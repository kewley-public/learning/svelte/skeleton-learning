import { t } from '$lib/trpc/t';
import { logger } from '$lib/trpc/middleware/logger';
import { z } from 'zod';
import prisma from '$lib/prisma';
import { delay } from '$lib/util';

export const pokemon = t.router({
	list: t.procedure
		.use(logger)
		.input(z.object({ sort: z.string(), sortBy: z.string() }).optional().nullable())
		.query(({ input }) => {
			return prisma.pokemon.findMany({
				select: {
					id: true,
					name: true
				},
				orderBy: input ? { [input.sortBy]: input.sort } : { id: 'desc' }
			});
		}),

	load: t.procedure
		.use(logger)
		.input(z.string())
		.query(async ({ input }) => {
			await delay(2000);
			return prisma.pokemon.findUniqueOrThrow({
				select: {
					id: true,
					name: true,
					types: true,
					moves: true,
					frontSpriteUrl: true,
					backSpriteUrl: true,
					frontShinyUrl: true,
					backShinyUrl: true
				},
				where: { name: input }
			});
		}),

	save: t.procedure
		.use(logger)
		.input(
			z.object({
				id: z.string().nullable().optional(),
				name: z.string(),
				moves: z.array(
					z.object({
						name: z.string()
					})
				),
				types: z.array(
					z.object({
						name: z.string()
					})
				),
				frontSpriteUrl: z.string().nullable().optional(),
				backSpriteUrl: z.string().nullable().optional(),
				frontShinyUrl: z.string().nullable().optional(),
				backShinyUrl: z.string().nullable().optional()
			})
		)
		.mutation(
			async ({
				input: {
					id,
					types,
					moves,
					name,
					frontSpriteUrl,
					backSpriteUrl,
					frontShinyUrl,
					backShinyUrl
				},
				ctx: { userId }
			}) => {
				console.info(userId);
				if (id) {
					throw Error('Update not supported yet');
				}

				const typeNames = types.map((t) => t.name);
				let typeRecords = await prisma.type.findMany({ where: { name: { in: typeNames } } });
				const existingNames = new Set(typeRecords.map((t) => t.name));
				const newTypeRecords = await Promise.all(
					typeNames
						.filter((name) => !existingNames.has(name))
						.map(async (name) => await prisma.type.create({ data: { name } }))
				);

				typeRecords = [...typeRecords, ...newTypeRecords];

				const moveNames = moves.map((m) => m.name);
				let moveRecords = await prisma.move.findMany({ where: { name: { in: moveNames } } });
				const existingMoveNames = new Set(moveRecords.map((m) => m.name));
				const newMoveRecords = await Promise.all(
					moveNames
						.filter((name) => !existingMoveNames.has(name))
						.map(async (name) => await prisma.move.create({ data: { name } }))
				);

				moveRecords = [...moveRecords, ...newMoveRecords];

				const pokemonId = crypto.randomUUID();

				const pokemon = await prisma.pokemon.create({
					data: {
						id: pokemonId,
						name,
						frontSpriteUrl,
						backSpriteUrl,
						frontShinyUrl,
						backShinyUrl,
						types: {
							connectOrCreate: typeRecords.map((t) => ({
								where: { typeId: t.id, pokemonId_typeId: { pokemonId, typeId: t.id } },
								create: { type: { connect: { id: t.id } } }
							}))
						},
						moves: {
							connect: moveRecords.map((move) => ({ id: move.id }))
						}
					},
					include: {
						types: {
							select: {
								type: {
									select: {
										name: true
									}
								}
							}
						},
						moves: true
					}
				});

				console.info('Saved Pokemon!', pokemon);
				return pokemon;
			}
		)
});
