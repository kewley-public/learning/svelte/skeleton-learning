import type { RequestEvent } from '@sveltejs/kit';
import jwt from 'jsonwebtoken';

export async function createContext(event: RequestEvent) {
	try {
		const token = event.cookies.get('jwt');
		const { id: userId } = jwt.verify(token || '', 'supersecret') as { id: string };
		return { userId };
	} catch {
		return { userId: '' };
	}
}

export type Context = Awaited<ReturnType<typeof createContext>>;
