import type { Router } from '$lib/trpc/router';
import { createTRPCClient, type TRPCClientInit } from 'trpc-sveltekit';
import transformer from 'trpc-transformer';

let browserClient: ReturnType<typeof createTRPCClient<Router>>;

export function trpc(init?: TRPCClientInit) {
	const isBrowser = typeof window !== 'undefined';
	if (isBrowser && browserClient) return browserClient;
	console.info(transformer);
	const client = createTRPCClient<Router>({ init, transformer });
	if (isBrowser) browserClient = client;
	console.info(client);
	return client;
}
