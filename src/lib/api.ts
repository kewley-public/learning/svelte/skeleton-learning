import { PokemonClient } from 'pokenode-ts';

const pokemonClient = new PokemonClient();

export const api = () => ({
	loadPokemonData: async (limit: number, offset: number = 0) =>
		await pokemonClient.listPokemons(offset, limit),
	getPokemonByName: async (name: string) => await pokemonClient.getPokemonByName(name)
});
