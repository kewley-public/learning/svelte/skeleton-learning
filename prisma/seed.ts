import { PrismaClient } from '@prisma/client';
import { PokemonClient } from 'pokenode-ts';

const pokemonClient = new PokemonClient();

const prisma = new PrismaClient();

async function main() {
	await prisma.pokemon.deleteMany();
	await prisma.move.deleteMany();
	await prisma.type.deleteMany();

	const bulbasaur = await pokemonClient.getPokemonByName('bulbasaur');
	const { name, types, moves, sprites } = bulbasaur;
	const pokemon = {
		name,
		types: types.map(({ type }) => ({ name: type.name })),
		moves: moves.map(({ move }) => ({ name: move.name }))
	};

	const frontSpriteUrl = sprites.front_default;
	const backSpriteUrl = sprites.back_default;
	const frontShinyUrl = sprites.front_shiny;
	const backShinyUrl = sprites.back_shiny;

	console.info(frontSpriteUrl, backSpriteUrl, frontShinyUrl, backShinyUrl);

	const typeCreates = pokemon.types.map(
		async ({ name }) => await prisma.type.create({ data: { name } })
	);
	const typeRecords = await Promise.all(typeCreates);

	const moveCreates = pokemon.moves.map(
		async ({ name }) => await prisma.move.create({ data: { name } })
	);
	const moveRecords = await Promise.all(moveCreates);

	const pokemonId = crypto.randomUUID();
	const pokemonRecord = await prisma.pokemon.create({
		data: {
			id: pokemonId,
			name,
			frontSpriteUrl,
			backSpriteUrl,
			frontShinyUrl,
			backShinyUrl,
			moves: {
				connect: moveRecords.map((m) => ({ id: m.id }))
			},
			types: {
				connectOrCreate: typeRecords.map((t) => ({
					where: { typeId: t.id, pokemonId_typeId: { pokemonId, typeId: t.id } },
					create: { type: { connect: { id: t.id } } }
				}))
			}
		},
		include: {
			moves: true,
			types: {
				select: {
					type: {
						select: {
							name: true
						}
					}
				}
			}
		}
	});

	console.info('Pokemon Record', pokemonRecord);
}

main()
	.then(async () => {
		await prisma.$disconnect();
	})
	.catch((e) => {
		console.error(e);
		throw e;
	})
	.finally(async () => {
		await prisma.$disconnect();
	});
