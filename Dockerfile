FROM node:bookworm-slim as node_modules

WORKDIR /app

COPY package.json ./

RUN npm install

FROM node:bookworm-slim as febuild
RUN apt-get update -y && apt-get install -y openssl
WORKDIR /app

COPY --from=node_modules /app/node_modules node_modules
COPY --from=node_modules /app/package.json package.json

COPY prisma/ prisma
COPY src/ src
COPY static/ static
COPY .env .
COPY svelte.config.js .
COPY tailwind.config.ts .
COPY tsconfig.json .
COPY vite.config.ts .
COPY .svelte-kit/ .svelte-kit
COPY postcss.config.cjs ./
# DB info
COPY data/ data

RUN npx prisma generate
RUN npx prisma migrate deploy
RUN npm run build

FROM node:bookworm-slim
RUN apt-get update -y && apt-get install -y openssl
WORKDIR /app
COPY --from=febuild /app .

CMD ["node", "build"]